package HW4;


import java.util.Scanner;

public class HappyFamily {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        Human h1 = new Human("Beth", "Smith", 1975, 60,"jane","jack");
        Pet p1 = new Pet("Octopus", "SquidWard", 3, 50, new String[]{"eat","play","sleep"});


        System.out.println();
        System.out.println(p1);
        System.out.println(h1);
        System.out.println(p1.eat());
        System.out.println(p1.foul());
        h1.GreetPet(p1.nickname);
        p1.respond();
        h1.FeedPet(p1.tricklevel,p1.species);
        h1.DescribePet(p1.species, p1.age);
    }
}
