package HW4;

import java.util.Random;

public class Human extends Pet {
    Random r = new Random();
    int pseudorandom = r.nextInt()%100;
    String name;
    String surname;
    int year;
    int iq;
    String mother;
    String father;

    public Human(String name, String surname, int year, int iq, String mother, String father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mother = mother;
        this.father = father;

    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human() {
    }

    public void GreetPet(String nickname ){
        System.out.println("Hello, " + nickname);
    }
    public void DescribePet(String species, int age){
        System.out.printf("I have %s, he is %d years old, he is slow", species, age);
    }
    public void FeedPet(int tricklevel, String species){
        if(pseudorandom > tricklevel) System.out.println("I think " + species +" is not hungry");
        else System.out.println("Hm... I will feed Jack's " + species);
    }

    @Override
    public String toString() {
        return String.format("Human {name='%s', surname='%s', year=%d, iq=%d, mother='%s', father='%s', }", name, surname, year, iq, mother, father);
    }
}
