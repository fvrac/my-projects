package HW4;

import java.util.Arrays;

public class Pet {
    public String species;
    public String nickname;
    public int age;
    public int tricklevel;
    String eat;
    String[] habits;


    public String eat(){
        return "I am eating";
    }

    public String foul(){
        return "I need to cover it up";
    }

    public void respond(){
        System.out.println("Hello, owner. I am " + nickname + ". I miss you!");
    }

    public Pet() {
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int tricklevel, String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.tricklevel = tricklevel;
        this.habits = habits;
    }


    @Override
    public String toString() {
        return String.format("%s {nickname='%s', age=%d, trickLevel=%d, habits=%s}", species,nickname,age,tricklevel, Arrays.toString(habits));
    }
}
