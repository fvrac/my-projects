package ClassWork;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class RandomStringWriteToFile {


    public static String name() {
        Random r = new Random();
        StringBuilder s = new StringBuilder();
        char ch;
        int n;
        int rand = r.nextInt(30);
        if (rand < 10) rand += 10;
        for (int i = 0; i < rand; i++) {
            n = r.nextInt(26) + 65;
            ch = ((char) n);
            s.append(ch);
        }
        return String.valueOf(s);
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("./data", "names1.txt");
        File file = new File("./data", "names1.txt");
        Path paths = Paths.get("./data", "names2.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        ArrayList<String> names = new ArrayList<>();

        for (int i = 0; i < 30; i++) {
            names.add(name());
        }

        Files.write(path, names);

        String line;
        while ((line=br.readLine())!=null) {
            System.out.println(line);
        }


        Files.write(paths, names);

    }
}
