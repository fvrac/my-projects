package ClassWork;

import java.util.*;

public class RandomString {

    private static String generate_string() {
        Random r = new Random();
        StringBuilder s = new StringBuilder();
        char c;
        int n;
        int x = r.nextInt(30);
        if (x < 10) x = x + 10;
        for (int i = 0; i < x; i++) {
            n = r.nextInt(26) + 65;
            c = (char) n;
            s.append(c);
        }
        return s.toString();
    }

    public static void main(String[] args) {
        Random r = new Random();
        Set<Integer> hash_Set = new HashSet<>();
        Map<Integer, String> hash_map = new HashMap<>();
        int n, i = 1;

        while (hash_Set.size() < 20) {
            n = r.nextInt() % 11;
            hash_Set.add(n);
        }
        System.out.println(hash_Set);

        while (hash_map.size() < 20) {
            hash_map.put(i++, generate_string());
        }

        System.out.println(hash_map);

    }
}
