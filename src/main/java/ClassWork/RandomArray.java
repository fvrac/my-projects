package ClassWork;

        import java.util.Random;
        import java.util.Scanner;

public class RandomArray {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random r = new Random();
        int[] A = new int[20];

        for (int i = 0; i < 20; i++) {
            A[i] = r.nextInt(10);
        }

        for (int i = 0; i < 20; i++) {
            System.out.print(A[i] + " ");
        }

        System.out.println();

        for (int i = 0; i < 20; i++) {
            if (A[i] % 2 == 0 && A[i] % 3 == 0) System.out.print("FizzBuzz ");
            else if (A[i] % 3 == 0) System.out.print("Buzz ");
            else if (A[i] % 2 == 0) System.out.print("Fizz ");
            else System.out.print("  ");
        }

    }
}
