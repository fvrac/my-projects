package ClassWork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Arraylist {
    public static void main(String[] args) {
        Random r = new Random();
        ArrayList<Integer> ArrList = new ArrayList<Integer>();
        ArrayList<Integer> ArrListOdd = new ArrayList<Integer>();
        ArrayList<Integer> ArrListEven = new ArrayList<Integer>();
        for (int i = 0; i < 100; i++) {
            int n = r.nextInt(100);
            ArrList.add(n);
            if (n % 2 == 0) {
                ArrListEven.add(n);
            } else {
                ArrListOdd.add(n);
            }
        }
        System.out.println(Arrays.deepToString(ArrList.toArray()));
        System.out.println(Arrays.deepToString(ArrListEven.toArray()));
        System.out.println(Arrays.deepToString(ArrListOdd.toArray()));
    }
}
